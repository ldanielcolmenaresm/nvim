# To use without any problem, we must have these development tools installed
* Node.js , nvm
* extensi  CoCInstall
* python2 python3
* Git
* NerdFonts

* Modules python neoVim *

	python -m pip uninstall neovim pynvim
	python -m pip install --user --upgrade pynvim   ==> Ejecutar en la terminal 

	nota: use with snippets

* Extensiones Coc *

	CocInstall
	coc-tabnine
	coc-html-css-support
	coc-json
	coc-prettier
	coc-python
	coc-scssmodules
	coc-sh
	coc-snippets
	coc-tsserver
	coc-webpack
	coc-emoji
