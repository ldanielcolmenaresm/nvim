" Paths
so ~/.config/nvim/plugins.vim
so ~/.config/nvim/plugin-config.vim
so ~/.config/nvim/maps.vim

set number
set mouse=a
set numberwidth=1
set clipboard=unnamed
set clipboard=unnamedplus
syntax on
set showcmd
set ruler
set cursorline
set encoding=utf-8
set showmatch
set sw=2
set relativenumber
set winblend=10
set pumblend=5
set nowrap
set lazyredraw
set wildmenu  " usar el tab en barra search

" theme Oceanic next
colorscheme OceanicNext
let g:airline_theme='oceanicnext'
" Or if you have Neovim >= 0.1.5
if (has("termguicolors"))
 set termguicolors
endif

 " theme gruvbox
"colorscheme gruvbox
"let g:gruvbox_contrast_dark = "dark" 

"highlight Normal ctermbg=NONE
set laststatus=2
set noshowmode

"" Searching
set hlsearch                    " highlight matches
set incsearch                   " incremental searching
set ignorecase                  " searches are case insensitive...
set smartcase                   " ... unless they contain at least one capital letter
